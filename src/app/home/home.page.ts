import { Component } from '@angular/core';
import { TopFiveService } from '../service/top-five.service';
import { TopFiveMovieComponents, MovieListItem, OrderSelectItem, OrderSelectComponent, MovieComponent } from '../model/top-five-components';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../model/app-state';
import * as TopFiveActions from './../actions/top-five.actions';
import { LoadingService } from '../service/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  orderSortTypes: OrderSelectItem[];
  movieList: MovieListItem[];
  orderBy: string;

  constructor(
    private topFive: TopFiveService,
    private router: Router,
    private store: Store<AppState>,
    public loading: LoadingService
    ) {}

  ionViewWillEnter() {
    this.fetchMovies();
  }

  fetchMovies() {
    this.store.select('topFiveMovieComponents').subscribe(
      ((storeResult) => {
        if (storeResult.length === 0) {
          // this.loading.present();
          this.topFive.getTopFive().subscribe(
            ((serviceResult: TopFiveMovieComponents) => {
              const movieListComp = serviceResult.components.find(comp => comp.type === 'movie-list') as MovieComponent;
              this.movieList = movieListComp.items;

              const orderSelectComp = serviceResult.components.find(comp => comp.type === 'order-select') as OrderSelectComponent;
              this.orderSortTypes = orderSelectComp.items;

              this.store.dispatch(new TopFiveActions.StoreTopFive([serviceResult]));

              this.orderBy = this.orderSortTypes[1].valueToOrderBy;
              this.orderMovies(this.orderBy, this.movieList);
              // this.loading.dismiss();
            }),
            (error => {
              console.log(error);
              // this.loading.dismiss();
            })
          );
        } else {
          const topFiveComponent = storeResult.find(comp => comp.type === 'top-5-movies');

          const movieListComp = topFiveComponent.components.find(comp => comp.type === 'movie-list') as MovieComponent;
          this.movieList = movieListComp.items;

          const orderSelectComp = topFiveComponent.components.find(comp => comp.type === 'order-select') as OrderSelectComponent;
          this.orderSortTypes = orderSelectComp.items;

          this.orderBy = this.orderSortTypes[1].valueToOrderBy;
          this.orderMovies(this.orderBy, this.movieList);
          this.resetShow(this.movieList);
          // this.loading.dismiss();
        }
      })
    );
  }

  showSynopsis(movieId: number) {
    this.movieList.forEach(movie => {
      if (movie.id === movieId) {
        movie.show = !movie.show;
      } else {
        movie.show = false;
      }
    });
  }

  showMovie(movieRank: number) {
    this.router.navigate(['/top-five/movie/' + movieRank]);
  }

  orderMovies(by: string, movieList: MovieListItem[], reverse?: boolean) {
    if (!reverse) {
      movieList.sort((a, b) => {
        if (a[by] > b[by]) {
          return 1;
        }
      });
    } else {
      movieList.sort((a, b) => {
        if (a[by] < b[by]) {
          return 1;
        }
      });
    }
  }

  onSortOptionChange() {
    this.orderMovies(this.orderBy, this.movieList);
  }

  addNewMovie() {
    const tempMovieList: MovieListItem[] = JSON.parse(JSON.stringify(this.movieList));
    this.orderMovies('id', tempMovieList, true);
    this.router.navigate(['top-five/add-movie/' + (++tempMovieList[0].id)]);
  }

  resetShow(movieList: MovieListItem[]) {
    movieList.forEach(item => {
      if (item.show) {
        item.show = false;
      }
    });
  }
}
