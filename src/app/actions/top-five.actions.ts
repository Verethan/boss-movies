import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { MovieListItem, TopFiveMovieComponents } from '../model/top-five-components';

export const STORE_TOP_FIVE = '[TOP-FIVE] Store All Movies';
export const UPDATE_TOP_FIVE = '[TOP-FIVE] Update Existing Movie';
export const ADD_TOP_FIVE = '[TOP-FIVE] Add Movie';

export class StoreTopFive implements Action {
    readonly type = STORE_TOP_FIVE;

    constructor(public payload: TopFiveMovieComponents[]) {}
}

export class UpdateTopFive implements Action {
    readonly type = UPDATE_TOP_FIVE;

    constructor(public payload: MovieListItem) {}
}

export class AddTopFive implements Action {
    readonly type = ADD_TOP_FIVE;

    constructor(public payload: MovieListItem) {}
}

export type Actions = StoreTopFive | UpdateTopFive | AddTopFive;
