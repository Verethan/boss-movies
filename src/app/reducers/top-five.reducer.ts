import { Action } from '@ngrx/store';
import * as TopFiveActions from './../actions/top-five.actions';
import { MovieListItem, TopFiveMovieComponents, MovieComponent } from '../model/top-five-components';

const initialState: TopFiveMovieComponents[] = [];

export function topFiveReducer(state: TopFiveMovieComponents[] = initialState, action: TopFiveActions.Actions) {

    switch (action.type) {

        case TopFiveActions.STORE_TOP_FIVE:
            return [...action.payload];

        case TopFiveActions.ADD_TOP_FIVE:
            const newState1 = JSON.parse(JSON.stringify(state));
            const components1 = newState1.find(comp => comp.type === 'top-5-movies').components;
            const movieComponent1: MovieComponent = components1.find(comp => comp.type === 'movie-list') as MovieComponent;
            const movieList1: MovieListItem[] = movieComponent1.items;
            let indexRankFive1: number;
            let index1 = 0;
            movieList1.forEach(item => {
                if (item.rank >= action.payload.rank && item.rank < 5) {
                    item.rank++;
                } else if (item.rank === 5) {
                    indexRankFive1 = index1;
                }
                index1++;
            });
            movieList1.splice(indexRankFive1, 1);
            movieList1.push(action.payload);
            return [...newState1];

        case TopFiveActions.UPDATE_TOP_FIVE:
            const newState2 = JSON.parse(JSON.stringify(state));
            const components2 = newState2.find(comp => comp.type === 'top-5-movies').components;
            const movieComponent2: MovieComponent = components2.find(comp => comp.type === 'movie-list') as MovieComponent;
            const movieList2: MovieListItem[] = movieComponent2.items;
            movieList2.forEach(item => {
                if (item.id === action.payload.id) {
                    item = action.payload;
                } else if (item.rank >= action.payload.rank && item.rank < 5) {
                    item.rank++;
                }
            });
            return [...newState2];

        default:
            return state;
    }
}
