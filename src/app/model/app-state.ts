import { MovieListItem, OrderSelectItem, TopFiveMovieComponents } from './top-five-components';

export interface AppState {
  readonly topFiveMovieComponents: TopFiveMovieComponents[];
}
