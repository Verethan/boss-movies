export interface TopFiveMovieComponents {
    type: string;
    components: (OrderSelectComponent | MovieComponent)[];
}

export interface OrderSelectComponent {
    type: string;
    items: OrderSelectItem[];
}

export interface MovieComponent {
    type: string;
    items: MovieListItem[];
}

export interface OrderSelectItem {
    label: string;
    valueToOrderBy: string;
}

export interface MovieListItem {
    id: number;
    type: string;
    rank: number;
    synopsis: string;
    title: string;
    imageUrl: string;
    releaseDate: string;
    show?: boolean;
}
