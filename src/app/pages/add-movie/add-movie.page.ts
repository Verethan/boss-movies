import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TopFiveService } from 'src/app/service/top-five.service';
import { TopFiveMovieComponents, MovieListItem, OrderSelectItem } from 'src/app/model/top-five-components';
import { Store } from '@ngrx/store';
import { AppState } from '../../model/app-state';
import * as TopFiveActions from '../../actions/top-five.actions';
import { LoadingService } from 'src/app/service/loading.service';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.page.html',
  styleUrls: ['./add-movie.page.scss'],
})
export class AddMoviePage {

  movie: MovieListItem = {
    id: null,
    type: 'poster',
    rank: null,
    synopsis: '',
    title: '',
    imageUrl: '',
    releaseDate: null
  };

  tempMovie: MovieListItem;
  editable = false;
  releaseDateSelection: number[] = [];
  rankSelection: number[] = [1, 2, 3, 4, 5];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private topFive: TopFiveService,
    private store: Store<AppState>,
    public loading: LoadingService
    ) { }

  ionViewWillEnter() {
    this.movie.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.tempMovie = JSON.parse(JSON.stringify(this.movie));
    this.releaseDateSelection = this.releaseDateRange();
  }

  goHome() {
    this.router.navigate(['/top-five']);
  }

  onSave() {
    this.editable = false;
    this.store.dispatch(new TopFiveActions.AddTopFive(this.movie));
    this.goHome();
  }

  onCancel() {
    this.editable = false;
    this.movie = JSON.parse(JSON.stringify(this.tempMovie));
  }

  formInvalid(): boolean {
    return (
      !this.movie.rank ||
      !this.movie.releaseDate ||
      this.movie.imageUrl.length === 0 ||
      this.movie.synopsis.length === 0
    );
  }

  releaseDateRange(): number[] {
    const releaseDateStart = 1950;
    const releaseDateEnd = new Date().getFullYear();
    const yearArray: number[] = [];
    for (let a = releaseDateStart; a <= releaseDateEnd; a++) {
      yearArray.push(a);
    }
    return yearArray;
  }
}
