import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TopFiveService } from 'src/app/service/top-five.service';
import { TopFiveMovieComponents, MovieListItem, OrderSelectItem } from 'src/app/model/top-five-components';
import { Store } from '@ngrx/store';
import { AppState } from '../../model/app-state';
import * as TopFiveActions from '../../actions/top-five.actions';
import { LoadingService } from 'src/app/service/loading.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.page.html',
  styleUrls: ['./movie.page.scss'],
})
export class MoviePage {

  movie: MovieListItem;
  tempMovie: MovieListItem;
  editable = false;
  releaseDateSelection: number[] = [];
  rankSelection: number[] = [1, 2, 3, 4, 5];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private topFive: TopFiveService,
    private store: Store<AppState>,
    public loading: LoadingService
    ) { }

  ionViewWillEnter() {
    this.releaseDateSelection = this.releaseDateRange();
    this.fetchMovie();
  }

  fetchMovie() {
    const movieRank = this.route.snapshot.paramMap.get('movieRank');
    // this.loading.present();
    this.store.select('topFiveMovieComponents').subscribe(
      ((storeResult) => {
        if (storeResult.length === 0) {
          this.topFive.getTopFive().subscribe(
            ((serviceResult: TopFiveMovieComponents) => {
              const movieList = serviceResult.components.find(component => component.type === 'movie-list').items as MovieListItem[];
              this.movie = movieList.find(movieItem => movieItem.rank.toString() === movieRank);
              this.store.dispatch(new TopFiveActions.StoreTopFive([serviceResult]));
              // this.loading.dismiss();
            }),
            (error => {
              console.log(error);
              // this.loading.dismiss();
            })
          );
        } else {
          const topFiveComponent = storeResult.find(comp => comp.type === 'top-5-movies');
          const movieList = topFiveComponent.components.find(component => component.type === 'movie-list').items as MovieListItem[];
          this.movie = movieList.find(movieItem => movieItem.rank.toString() === movieRank);
          // this.loading.dismiss();
        }
      })
    );
  }

  goHome() {
    this.router.navigate(['/top-five']);
  }

  onEdit() {
    this.editable = true;
    this.tempMovie = JSON.parse(JSON.stringify(this.movie));
  }

  onSave() {
    this.editable = false;
    this.store.dispatch(new TopFiveActions.UpdateTopFive(this.movie));
    this.goHome();
  }

  onCancel() {
    this.editable = false;
    this.movie = JSON.parse(JSON.stringify(this.tempMovie));
  }

  releaseDateRange(): number[] {
    const releaseDateStart = 1950;
    const releaseDateEnd = new Date().getFullYear();
    const yearArray: number[] = [];
    for (let a = releaseDateStart; a <= releaseDateEnd; a++) {
      yearArray.push(a);
    }
    return yearArray;
  }
}
