import { TestBed } from '@angular/core/testing';

import { TopFiveService } from './top-five.service';

describe('TopFiveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TopFiveService = TestBed.get(TopFiveService);
    expect(service).toBeTruthy();
  });
});
