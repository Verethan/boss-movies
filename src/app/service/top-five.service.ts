import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { TopFiveMovieComponents, OrderSelectItem, MovieListItem } from '../model/top-five-components';

@Injectable({
  providedIn: 'root'
})
export class TopFiveService {

  url = 'http://demo9595712.mockable.io/getTopFiveMovies';

  orderSelectTypes: OrderSelectItem[] = [];
  movieList: MovieListItem[];

  constructor(private http: HttpClient) { }

  getTopFive(): Observable<TopFiveMovieComponents> {
    return this.http.get<TopFiveMovieComponents>(this.url);
  }

}
