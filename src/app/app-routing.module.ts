import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'top-five', pathMatch: 'full' },
  { path: 'top-five', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'top-five/movie/:movieRank', loadChildren: './pages/movie/movie.module#MoviePageModule' },
  { path: 'top-five/add-movie/:id', loadChildren: './pages/add-movie/add-movie.module#AddMoviePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
